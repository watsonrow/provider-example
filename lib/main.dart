import 'dart:developer';

import 'package:flutter/cupertino.dart';
import 'package:http/http.dart' as http;
import 'package:flutter/material.dart';
import 'package:locator/countries.dart';
import 'package:provider/provider.dart';

import 'package:flutter/services.dart' show rootBundle;

import 'dart:convert';
import 'address.dart';

void main() async {
  final key_data = await rootBundle.loadString('assets/secrets.json');
  final keys = json.decode(key_data);
  runApp(
    ChangeNotifierProvider(
      builder: (context) => Address(),
      child: MyApp(keys),
    ),
  );
}

class MyApp extends StatelessWidget {
  dynamic _keys;

  MyApp(this._keys);

  @override
  Widget build(BuildContext context) {
    return CupertinoApp(title: 'Provider Demo', home: HomePage(this._keys));
  }
}

class HomePage extends StatelessWidget {
  dynamic _keys;

  HomePage(this._keys);

  @override
  Widget build(BuildContext context) {
    ;

    return CupertinoPageScaffold(
      navigationBar: CupertinoNavigationBar(
        middle: Text("Provider Example"),
      ),
      child: SafeArea(
        child: Padding(
          padding: EdgeInsets.only(top: 8.0),
          child: Column(
        children: <Widget>[
          Consumer<Address>(
            builder: (context, address, _) => MapWidget(address.coordinates, _keys['static_maps_key']),
          ),
          AddressWidget(),
          CupertinoButton(
            child: Text("Update Map"),
            onPressed: () => Provider.of<Address>(context, listen: false).geocodeAddress(context, _keys['geocoding_key']),
          )
        ],
      ),
      ),
      ),
    );
  }
}

class MapWidget extends StatelessWidget {
  Coordinates _coordinates;
  String _maps_key;

  MapWidget(this._coordinates, this._maps_key);

  @override
  Widget build(BuildContext context) {
    return Image.network('https://maps.googleapis.com/maps/api/staticmap?center=' +
        this._coordinates.latitude.toString() +
        ',' +
        this._coordinates.longitude.toString() +
        '&zoom=17&size=300x300&maptype=roadmap&markers=color:red%7Clabel:S%7C' +
        this._coordinates.latitude.toString() +
        ',' +
        this._coordinates.longitude.toString() +
        '&key=' +
        _maps_key);
  }
}

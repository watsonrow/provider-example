import 'dart:convert';
import 'dart:developer';

import 'package:flutter/cupertino.dart';
import 'package:flutter/services.dart';
import 'package:provider/provider.dart';
import 'package:http/http.dart' as http;

import 'countries.dart'; 

class AddressWidget extends StatelessWidget{

  @override
  Widget build(BuildContext context) {
    return _showAddressInput(context);
  }

  Widget _showAddressInput(BuildContext context){
    return Column(
        children: <Widget>[
          Padding(
            padding: EdgeInsets.fromLTRB(2, 8, 2, 3),
            child: Text(
              "Enter address"
              ),
          ),
           Row(
             children: <Widget>[
               _showStreetNumber(context),
               _showStreetName(context), 
            ],
           ),
           Row(
              children: <Widget>[
                _showCity(context), 
                _showState(context),
                _showZip(context)
           ],
          ),
         Center( 
             child: _showCountry(context),
          ),
        ]
    );
  }

  Widget _showStreetNumber(BuildContext context){
    return Expanded(
      flex: 1,
      child: new CupertinoTextField(
        placeholder: "No." ,
        maxLines: 1,
        keyboardType: TextInputType.number,
        autofocus: false,
        inputFormatters: [
            LengthLimitingTextInputFormatter(8),
        ],
        onChanged: (value) => Provider.of<Address>(context)._number = value, 
      ),
      );
  }

  Widget _showStreetName(BuildContext context){
    return Expanded(
      flex: 4,
      child: new CupertinoTextField(
        placeholder: "street name" ,
        maxLines: 1,
        keyboardType: TextInputType.text,
        autofocus: false,
        inputFormatters: [
            LengthLimitingTextInputFormatter(50),
        ],
        onChanged: (value) => Provider.of<Address>(context)._street = _format(value), 
      ),
      );
  }

  Widget _showCity(BuildContext context){
    return Expanded(
      flex: 4,
      child: new CupertinoTextField(
        placeholder: "city" ,
        maxLines: 1,
        keyboardType: TextInputType.text,
        autofocus: false,
        inputFormatters: [
            LengthLimitingTextInputFormatter(50),
        ],
        onChanged: (value) => Provider.of<Address>(context)._city = _format(value), 
      ),
      );
  }

  Widget _showState(BuildContext context){
    return Expanded(
      flex: 1,
      child: new CupertinoTextField(
        placeholder: "state" ,
        maxLines: 1,
        keyboardType: TextInputType.text,
        autofocus: false,
        inputFormatters: [
            LengthLimitingTextInputFormatter(3),
        ],
        onChanged: (value) => Provider.of<Address>(context)._state = _format(value), 
      ),
      );
  }

  Widget _showZip(BuildContext context){
    return Expanded(
      flex: 2,
      child: new CupertinoTextField(
        placeholder: "zip" ,
        maxLines: 1,
        keyboardType: TextInputType.text,
        autofocus: false,
        inputFormatters: [
            LengthLimitingTextInputFormatter(10),
        ],
        onChanged: (value) => Provider.of<Address>(context)._zip = value, 
      ),
      );
  }

  Widget _showCountry(BuildContext context){
    final FixedExtentScrollController _scrollController = 
        FixedExtentScrollController(initialItem: COUNTRIES.indexOf("United States"));
    return Padding(
      padding: EdgeInsets.fromLTRB(0.0, 2.0, 0.0, 0.0),
      child: _anchorPickerToBottom(
        CupertinoPicker(
        backgroundColor: CupertinoColors.white,
        scrollController: _scrollController, 
        itemExtent: 20.0,
        onSelectedItemChanged: (int index){
          Provider.of<Address>(context, listen: true)._country = _format(COUNTRIES[index]); 
        },
        children: List<Widget>.generate(COUNTRIES.length,  (int index) {
                  return Center(
                    child: Text(COUNTRIES[index]),
                  );
                }), 
      ),

      )
    );
  }
}

String _format(String string){
  return string.replaceAll(RegExp(r' '), '+'); 
}

Widget _anchorPickerToBottom(Widget picker){
  return Container(
    height: 216.0, 
    padding: const EdgeInsets.only(top: 6.0), 
    color: CupertinoColors.white,
    child: DefaultTextStyle(
      style: const TextStyle(
        color: CupertinoColors.black,
        fontSize: 22.0,
      ),
      child: picker, 
    ),
  ); 
}


class Address with ChangeNotifier {

  String _number; 
  String _street; 
  String _city; 
  String _state; 
  String _zip; 
  String _country; 
  Coordinates coordinates = Coordinates(0.0, 0.0); 

  Future<Coordinates> geocodeAddress(BuildContext context, String maps_key) async {

    final GEOCODING_BASE_URL = 'https://maps.googleapis.com/maps/api/geocode'; 
    final URL = GEOCODING_BASE_URL + '/json?address=' + this._number + '+' + this._street + ',' + this._city + ',' + this._state + '&key=' + maps_key;
    
    final headers = {
      "Content-Type": "application/json"
    };
    final response = await http.get(URL, headers:headers);

    if (response.statusCode == 200){
      this.coordinates = _parseBodyForCoordinates(response.body); 
    }
    notifyListeners(); 
  }

  Coordinates _parseBodyForCoordinates(String body){
    final data = json.decode(body); 
    final location = data['results'][0]['geometry']['location']; 
    return Coordinates(location['lat'], location['lng']);
  }
}
  
class Coordinates {

  double latitude; 
  double longitude; 

  Coordinates(this.latitude, this.longitude);
}











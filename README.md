# provider-example 

A sample Flutter project demonstrating the use of a ChangeNotifierProvider for state management. 

This application has one screen. Simply enter an address and then select `Update Map`. The map uses the entered address to update the displayed map image. 

## Getting Started

To run this sample project, you will need a MAPS api key capable or geocoding address as well as generating images from coordinates. Add those keys to `assets/secrets.json`. Google offers these features as seperate services. The geocoding feature is part of [Geocoding API](https://developers.google.com/maps/documentation/geocoding/start) while the second is part of the [Static Maps API](https://developers.google.com/maps/documentation/maps-static/intro).


![screenshot](assets/provider_example.png)

